'use strict';

const redis = require('redis');
const moment = require('moment');

// ---------------------- redis db connection ----------------

class RedisDB {
    constructor(config = {}) {
        this.client = redis.createClient(config);
        this.connect();
    }
    connect() {
        this.client.on('error', (err) => {
            console.log({ [moment.utc().format('DD/MMM HH:mm:ss')]: err.toString() });
        });
        this.client.on('connect', () => {
            // console.log(moment.utc().format('DD/MMM HH:mm:ss'), 'Redis client connected');
        });
    }
};

const get = {
    get(key = '') {
        return new Promise((resolve, reject) => {
            this.client.get(key, (err, result) => {
                if (err) {
                    // console.log({ [moment.utc().format('DD/MMM HH:mm:ss')]: err.toString() });
                    reject(err);
                };
                try {
                    resolve(JSON.parse(result));
                } catch (e) {
                    reject(err);
                }
            })
        });
    },
    del(key = [], errFunc, resFunc) {
        this.client.del(key, (err, result) => {
            if (err) errFunc(err);
            resFunc(result);
        });
    },
};

const set = {
    set(key, value, expirationTime = 0) {
        if (expirationTime === 0) {
            this.client.set(key, JSON.stringify(value), (err) => {
                if (err) {
                    console.log({ [moment.utc().format('DD/MMM HH:mm:ss')]: err.toString() });
                }
            });
        } else {
            this.client.set(key, JSON.stringify(value), 'EX', expirationTime, (err) => {
                if (err) {
                    console.log({ [moment.utc().format('DD/MMM HH:mm:ss')]: err.toString() });
                }
            });
        }
    },
    setSchema(key, value, expirationTime = 0) {
        if (expirationTime === 0) {
            this.client.set(key, value, (err) => {
                if (err) {
                    console.log({ [moment.utc().format('DD/MMM HH:mm:ss')]: err.toString() });
                }
            });
        } else {
            this.client.set(key, value, 'EX', expirationTime, (err) => {
                if (err) {
                    console.log({ [moment.utc().format('DD/MMM HH:mm:ss')]: err.toString() });
                }
            });
        }
    },
    incrbyfloat(key, increment, errCb = (e => e), resCb = (r => r)) {
        this.client.incrbyfloat(key, increment, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    expire(key, ttl, errCb = (e => e), resCb = (r => r)) {
        this.client.expire(key, ttl, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    expireat(key, timestamp, errCb = (e => e), resCb = (r => r)) {
        this.client.expireat(key, timestamp, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    persist(key, errCb = (e => e), resCb = (r => r)) {
        this.client.persist(key, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    ttl(key, errCb = (e => e), resCb = (r => r)) {
        this.client.ttl(key, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
};

const pub = {
    pub(channel = '', data) {
        this.client.publish(channel, JSON.stringify(data));
    },
    pubSchema(channel = '', data) {
        this.client.publish(channel, data);
    },
    pubStop() {
        this.client.quit();
    }
};

const sub = {
    sub(channel = '', errorCb = (e => e), resCb = (d => d)) {
        this.client.subscribe(channel);
        this.client.on("message", (cnl, msg) => {
            try {
                resCb(JSON.parse(msg));
            } catch (e) {
                errorCb(e);
            }
        });
    },
    unsub() {
        this.client.unsubscribe();
    }
};

const list = {
    // add to start of list
    lpush(listName, data, errCb = (e => e), resCb = (r => r)) {
        this.client.lpush(listName, data, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    // add to end of list
    rpush(listName, data, errCb = (e => e), resCb = (r => r)) {
        this.client.rpush(listName, data, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    // add to end of lint
    rpop(listName, count, errCb = (e => e), resCb = (r => r)) {
        this.client.rpop(listName, [count], (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    // get from list: start stop indexes
    lrange(listName, start, stop, errCb = (e => e), resCb = (r => r)) {
        this.client.lrange(listName, start, stop, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    // delete from list: start stop indexes 
    // use 1 -1 where 1 q of items 
    ltrim(listName, start, stop, errCb = (e => e), resCb = (r => r)) {
        this.client.ltrim(listName, start, stop, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    getFromList(listName, count, errCb = (e => e), resCb = (r => r)) {
        this.client.lrange(listName, 0, count, (err, listRes) => {
            if (err) errCb(err);
            this.client.ltrim(listName, count, -1, (err, trimRes) => {
                if (err) errCb(err);
                resCb(listRes);
            });
        });
    },
};

const hash = {
    hset(hashName, args = {}, errCb = (e => e), resCb = (r => r)) {
        const toSave = [];
        for (const [key, value] of Object.entries(args)) {
            toSave.push(key || 'undefined', value || value + '');
        };
        this.client.hset(hashName, toSave, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    hsetnx(hashName, args = {}, errCb = (e => e), resCb = (r => r)) {
        const toSave = [];
        for (const [key, value] of Object.entries(args)) {
            toSave.push(key || 'undefined', value || value + '');
        };
        this.client.hsetnx(hashName, toSave, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    hget(hashName, fields = [], errCb = (e => e), resCb = (r => r)) {
        this.client.hget(hashName, fields, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    hgetall(hashName, errCb = (e => e), resCb = (r => r)) {
        this.client.hgetall(hashName, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    hgetallpromise(hashName) {
        return new Promise((resolve, reject) => {
            this.client.hgetall(hashName, (err, res) => {
                if (err) reject(err);
                resolve(res);
            });
        });
    },
    hdel(hashName, fields = [], errCb = (e => e), resCb = (r => r)) {
        this.client.hdel(hashName, fields, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    hexists(hashName, fieldName = '', errCb = (e => e), resCb = (r => r)) {
        this.client.hexists(hashName, fieldName, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    hkeys(hashName, errCb = (e => e), resCb = (r => r)) {
        this.client.hkeys(hashName, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    hvals(hashName, errCb = (e => e), resCb = (r => r)) {
        this.client.hvals(hashName, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    hlen(hashName, errCb = (e => e), resCb = (r => r)) {
        this.client.hlen(hashName, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    hincrbyfloat(hashName, fieldName = '', value = 0, errCb = (e => e), resCb = (r => r)) {
        this.client.hincrbyfloat(hashName, fieldName, value, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    hincrby(hashName, fieldName = '', value = 0, errCb = (e => e), resCb = (r => r)) {
        this.client.hincrby(hashName, fieldName, value, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
};

const sortedSet = {
    zaddOB(setName, data = [], errCb = (e => e), resCb = (r => r)) {
        for (const { price, size } of data) {
            this.client.zadd(setName, size, price, (err, res) => {
                if (err) errCb(err);
            });
        };
        resCb({ zadd: 'ok' });
    },
    /* data = [score, member, ...] */
    zadd(setName, data = [], errCb = (e => e), resCb = (r => r)) {
        this.client.zadd(setName, data, (err, res) => {
            if (err) errCb(err);
            else resCb(res);
        });
    },
    removeZeroOB(setName, min = '-inf', max = 0, errCb = (e => e), resCb = (r => r)) {
        this.client.zremrangebyscore(setName, min, max, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    getCrossNames(first = '', second = '', errCb = (e => e), resCb = (r => r)) {
        this.client.zinter(2, first, second, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    removeKey(setName, key = '', errCb = (e => e), resCb = (r => r)) {
        this.client.zrem(setName, key, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    getAllSet(setName, count, errCb = (e => e), resCb = (r => r)) {
        this.client.zrange(setName, 0, count - 1, 'withscores', (err, res) => {
            if (err) errCb(err);
            if ((Array.isArray(res) && res.length === 0)) return resCb(null)
            resCb(res);
        });
    },
};

const sets = {
    sadd(setName = '', value = [], errCb = (e => e), resCb = (r => r)) {
        this.client.sadd(setName, value, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    smembers(setName = '', errCb = (e => e), resCb = (r => r)) {
        this.client.smembers(setName, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    srem(setName = '', value = [], errCb = (e => e), resCb = (r => r)) {
        this.client.srem(setName, value, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    }
};

const stream = {
    xadd(streamName = '', maxLength = 0, data = [], errCb = (e => e), resCb = (r => r)) {
        this.client.xadd(streamName, 'MAXLEN', '~', maxLength, '*', ...data, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
    xrevrange(streamName = '', count = 0, errCb = (e => e), resCb = (r => r)) {
        this.client.xrevrange(streamName, '+', '-', 'COUNT', count, (err, res) => {
            if (err) errCb(err);
            resCb(res);
        });
    },
};

const scan = {
    scanall(pattern, errCb = (e => e), resCb = (r => r)) {
        let cursor = '0';
        const count = '1000';
        let result = [];
        do {
            this.client.scan(`${cursor}`, "MATCH", `${pattern}`, "COUNT", `${count}`, (err, res) => {
                cursor = res[0];
                if (err) errCb(err);
                const keys = res[1];
                result = [...result, ...keys];
                if (cursor === '0') resCb(result);
            });
        } while (cursor !== '0');
        // const recursion = () => {
        //     this.client.scan(`${cursor}`, "MATCH", `${pattern}`, "COUNT", `${count}`, (err, res) => {
        //         if (err) errCb(err);
        //         cursor = res[0];
        //         const keys = res[1];
        //         result = [...result, ...keys];
        //         if (cursor === '0') resCb(result);
        //         else recursion();
        //     });
        // };
        // recursion();
    },
    scanallpromise(pattern) {
        return new Promise((resolve, reject) => {
            let cursor = '0';
            const count = '1000';
            let result = [];
            do {
                this.client.scan(`${cursor}`, "MATCH", `${pattern}`, "COUNT", `${count}`, (err, res) => {
                    cursor = res[0];
                    if (err) reject(err);
                    const keys = res[1];
                    result = [...result, ...keys];
                    if (cursor === '0') resolve(result);
                });
            } while (cursor !== '0');
        });
    },
};

Object.assign(RedisDB.prototype, get, set, pub, sub, list, hash, sortedSet, stream, sets, scan);

module.exports = RedisDB;